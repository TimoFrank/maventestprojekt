package com.meylemueller.test.maven;

public class Main {

	public static void main(String[] args) 
	{
		// Get a DescriptiveStatistics instance
		DescriptiveStatistics stats = new DescriptiveStatistics();

		// Add the data from the array
		for( int i = 0; i < 5; i++) {
		        stats.addValue(20);
		}

		// Compute some statistics
		double mean = stats.getMean();
		double std = stats.getStandardDeviation();
		double median = stats.getPercentile(50);
	}

}
